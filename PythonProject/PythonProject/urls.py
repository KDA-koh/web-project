from django.contrib import admin
from django.conf.urls import url
from django.urls import path
from django.contrib.auth import views as auth_views
from . import views


urlpatterns = [
    url(r'^$', views.base, name='base'),
    url(r'^account', views.account, name='account'),
    url(r'^index/', views.index, name='index'),
    path('team/', views.create, name='team'),
    url(r'^login/', views.page_login, name='page_login'),
    url(r'^proc_login/', views.proc_login, name='proc_login'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^password/$', views.change_password, name='change_password'),
    path('admin/', admin.site.urls),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
]