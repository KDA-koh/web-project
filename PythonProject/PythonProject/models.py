from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Team(models.Model):
    name = models.CharField(max_length=32)
    #player = models.ManyToManyField(Player)

    class Meta:
        db_table = "Team"


class Player(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, null=True, on_delete=models.CASCADE)

    class Meta:
        db_table = "Player"


@receiver(post_save, sender=User)
def new_user(sender, instance, created, **kwargs):
    if created:
        Player.objects.create(user=instance)
        instance.player.save()


class Tournament(models.Model):
    name = models.CharField(max_length=32)
    date = models.DateField()

    class Meta:
        db_table = "Tournament"


class Match(models.Model):
    date = models.DateField()
    result = models.CharField(max_length=32)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)

    class Meta:
        db_table = "Match"