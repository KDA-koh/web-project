from django.http import *
from django.template import loader
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from .models import Player,Team


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('page_login')
    else:
        form = UserCreationForm()
        return render(request, 'signup.html', {'form': form})


def create_team(request):
    pass


def base(request):
    return render(request, "base.html")


def page_login(request):
    template = loader.get_template("login.html")
    return HttpResponse(template.render())


def proc_login(request):
    user_name = request.GET['user_name']
    password = request.GET['password']
    user = authenticate(username=user_name, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect("/")
        else:
            return HttpResponseRedirect("/login/")
    return HttpResponseRedirect("/login/")


def account(request):
    player = Player.objects.all()
    return render(request, "account.html", {'player': player})


def index(request):
    team = Team.objects.all()
    return render(request, "account.html", {'team': team})


def create(request):
    if request.method == "POST":
        t = Team()
        t.name = request.POST.get("name")
        t.save()
    return HttpResponseRedirect("/account/")


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Your password was successfully updated!')
            return redirect('change_password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'change_password.html', {'form': form})
