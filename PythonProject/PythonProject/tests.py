from django.test import TestCase

from PythonProject.models import *
from django.contrib.auth.models import User


class TestPlayersTeams(TestCase):
    def setUp(self):
        User.objects.create(username="Vanya228", first_name="Vanya", email="vanya@mail.ru", password="Vanya1")
        User.objects.create(username="Vasya337", first_name="Vasya", email="vasya@mail.ru", password="Vasya1")
        OG = Team.objects.create(name="OG")
        Astralis = Team.objects.create(name="Astralis")
        OG.player_set.add(Player.objects.get(user=5))
        Astralis.player_set.add(Player.objects.get(user=6))

    def test_check_teams(self):
        player1 = Player.objects.get(team=Team.objects.get(name="OG"))
        player2 = Player.objects.get(team=Team.objects.get(name="Astralis"))
        self.assertEqual(player1.user.id, 5)
        self.assertEqual(player2.user.id, 6)


class TestTournamentMatch(TestCase):
    def setUp(self):
        Team.objects.create(name="OG")
        Team.objects.create(name="Newbee")
        Team.objects.create(name="Astralis")
        Team.objects.create(name="Fnatic")
        Tournament.objects.create(name="Tour Dota", date="2020-10-10")
        Tournament.objects.create(name="Tour CSGO", date="2020-11-11")
        Match.objects.create(date="2020-10-10", result="1:0", team=Team.objects.get(name="OG"),
                             tournament=Tournament.objects.get(name="Tour Dota"))
        Match.objects.create(date="2020-10-11", result="1:0", team=Team.objects.get(name="OG"),
                             tournament=Tournament.objects.get(name="Tour Dota"))
        Match.objects.create(date="2020-11-11", result="1:0", team=Team.objects.get(name="Astralis"),
                             tournament=Tournament.objects.get(name="Tour CSGO"))
        Match.objects.create(date="2020-11-12", result="0:1", team=Team.objects.get(name="Fnatic"),
                             tournament=Tournament.objects.get(name="Tour CSGO"))

    def test_count_wins(self):
        WinsMatchOG = Match.objects.filter(team=3).count()
        WinsMatchNewbee = Match.objects.filter(team=4).count()
        WinsMatchAstralis = Match.objects.filter(team=5).count()
        WinsMatchFnatic = Match.objects.filter(team=6).count()
        self.assertEqual(WinsMatchOG, 2)
        self.assertEqual(WinsMatchNewbee, 0)
        self.assertEqual(WinsMatchAstralis, 1)
        self.assertEqual(WinsMatchFnatic, 1)


class UsersLoginTest(TestCase):
    def setUp(self):
        test_user1 = User.objects.create_user(username='test_user1', email='test1@mail.ru', password='Im_test_user')
        test_user2 = User.objects.create_user(username='test_user2', email='test2@mail.ru', password='227002')
        test_admin = User.objects.create_user(username='TestAdmin', email='testadmin@mail.ru',
                                              password='Im_test_admin', is_superuser=1, is_staff=1)
        test_not_admin = User.objects.create_user(username='NotAdmin', email='notadmin@mail.ru',
                                                  password='Im_not_admin', is_superuser=0, is_staff=0)

    def test_login_users(self):
        login1 = (self.client.login(username='test_user1', password='Im_test_user'))
        self.assertTrue(login1)
        login2 = (self.client.login(username='test_user2', password='228003'))
        self.assertFalse(login2)

    def test_login_admin(self):
        self.client.login(username='TestAdmin', password='Im_test_admin')
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 200)

    def test_login_not_admin(self):
        self.client.login(username='NotAdmin', password='Im_not_admin')
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 302)


class PasswordChangeCheck(TestCase):
    def setUp(self):
        test_user = User.objects.create_user(username='test_user', email='testu@mail.ru', password='old_pass_user')
        test_admin = User.objects.create_user(username='test_admin', email='testa@mail.ru', password='old_pass_admin',
                                              is_superuser=1, is_staff=1)

    def test_new_password_user(self):
        tu = User.objects.get(username='test_user')
        tu.set_password('new_pass_user')
        tu.save()
        self.assertFalse(self.client.login(username='test_user', password='old_pass_user'))
        self.assertTrue(self.client.login(username='test_user', password='new_pass_user'))

    def test_new_password_admin(self):
        ta = User.objects.get(username='test_admin')
        ta.set_password('new_pass_admin')
        ta.save()
        self.client.login(username='test_admin', password='old_pass_admin')
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 302)
        self.client.login(username='test_admin', password='new_pass_admin')
        response = self.client.get('/admin/')
        self.assertEqual(response.status_code, 200)
