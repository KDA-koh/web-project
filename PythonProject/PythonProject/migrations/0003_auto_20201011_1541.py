# Generated by Django 3.1.2 on 2020-10-11 10:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PythonProject', '0002_match_player_tournament'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='match',
            table='Match',
        ),
        migrations.AlterModelTable(
            name='player',
            table='Player',
        ),
        migrations.AlterModelTable(
            name='team',
            table='Team',
        ),
        migrations.AlterModelTable(
            name='tournament',
            table='Tournament',
        ),
    ]
